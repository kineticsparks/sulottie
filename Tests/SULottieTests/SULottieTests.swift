import XCTest
@testable import SULottie

final class SULottieTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(SULottie().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
