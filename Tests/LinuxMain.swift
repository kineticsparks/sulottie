import XCTest

import SULottieTests

var tests = [XCTestCaseEntry]()
tests += SULottieTests.allTests()
XCTMain(tests)
